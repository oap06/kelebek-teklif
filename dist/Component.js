sap.ui.define(["sap/ui/core/UIComponent","sap/ui/Device","zcrm/zcrm_proposal_operation/model/models","sap/ui/model/json/JSONModel","sap/ui/model/Filter","sap/ui/model/FilterOperator","zcrm/zcrm_proposal_operation/controller/FragmentController"],function(e,t,i,r,o,s,n){"use strict";var a=["__metadata","__deferred"];function l(e){if(Array.isArray(e)){return[]}if(e&&typeof e==="object"&&Array.isArray(e.results)){return{results:[]}}if(e instanceof Date){return new Date}if(typeof e==="string"&&e.match(/^\d{8}-\d{4}-\d{4}-\d{4}-\d{12}/)){return"00000000-0000-0000-0000-000000000000"}var t={object:{},string:"",number:0,boolean:false,undefined:undefined,function:function(){}};return t[typeof e]}return e.extend("zcrm.zcrm_proposal_operation.Component",{metadata:{manifest:"json"},init:function(){e.prototype.init.apply(this,arguments);this.getRouter().initialize();this.setModel(i.createDeviceModel(),"device");this.setModel(new r({}),"local");this.fragments={};this.setModel(new r({}),"search");this.setModel(new r({}),"headersearch");this.setModel(new r({}),"cheadersearch");this.setModel(new r({}),"selected");this.setModel(new r({}),"actionResultMessage");this.setModel(new r({}),"KampanyaBelirle");this.setModel(new r({}),"oToplamModel");this.setModel(new r({}),"visible");this.getModel("headersearch").setSizeLimit(99999999);this.getModel("cheadersearch").setSizeLimit(99999999);this.checkFromOtherApps()},exit:function(){Object.keys(this.fragments).forEach(function e(t){this.fragments[t].oDialog.destroy();delete this.fragments[t]}.bind(this))},openFragment:function(e,t){if(!this.fragments[e]){var i=this.byId(this.getMetadata().getRootView().id);var r=new n;var o=[this.getMetadata().getComponentName(),"fragments",e].join(".");this.fragments[e]={oDialog:sap.ui.xmlfragment(o,r),oController:r};i.addDependent(this.fragments[e].oDialog);r.setDialog(this.fragments[e].oDialog);r.setView(t)}this.fragments[e].oDialog.open();return this.fragments[e].oController},checkFromOtherApps:function(){var e=sap.ui.getCore().getModel("crossData");if(!e){return}var t=e.getProperty("/sendToTeklif");sap.ui.getCore().setModel(null,"crossData");if(t){sap.ui.getCore().setModel(new r(t),"oModelGlobal");if(t.mode==="display"){this.getRouter().navTo("detail",{guid:t.Guid},true)}else{this.getRouter().navTo("CreateEdit",{guid:t.Guid,orderType:t.ProcessType},true)}}},resetModel:function(e,t,i){Object.entries(e).forEach(function(r){var o=r[0];var s=r[1];if(o in t||a.includes(o)){return}else if(i.includes(o)){t[o]=e[o]}else{t[o]=l(s)}});return t},resetFields:function(e,t,i){Object.entries(e).forEach(function(r){var o=r[0];var s=r[1];if(o in t||a.includes(o)){return}else if(i.includes(o)){t[o]=l(s)}else{t[o]=e[o]}});return t}})});