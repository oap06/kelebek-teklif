sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/core/BusyIndicator"
], function (JSONModel, Filter, FilterOperator, BusyIndicator) {
	"use strict";

	function FragmentController() {}

	Object.assign(FragmentController.prototype, {
		setDialog: function (oDialog) {
			this.oDialog = oDialog;
		},
		setView: function (oView) {
			this.oView = oView;
		},
		setModel: function (oModel, sModelName) {
			this.getDialog().setModel(oModel, sModelName);
		},
		setSource: function (oControl) {
			//eslint-disable-next-line sap-no-ui5base-prop
			this.oSource = oControl;
		},
		getOwnerComponent: function () {
			return this.getView().getController().getOwnerComponent();
		},
		getView: function () {
			return this.oView;
		},
		getSource: function () {
			//eslint-disable-next-line sap-no-ui5base-prop
			return this.oSource;
		},
		getDialog: function () {
			return this.oDialog;
		},
		getOwnerModelProperty: function (oModel, sPath) {
			return this.getOwnerComponent().getModel(oModel).getProperty(sPath);
		},
		setOwnerModelProperty: function (oModel, sPath, value) {
			return this.getOwnerComponent().getModel(oModel).setProperty(sPath, value);
		},
		log: function (oEvent) {
			// eslint-disable-next-line
			console.log("log triggered");
		},
		open: function (sFragmentName) {
			return this.getView().getController().open(sFragmentName);
		},
		onClose: function (oEvent) {
			this.oDialog.close();
		},
		onCloseCat: function (oEvent) {
			this.oDialog.close();
			this.getView().getController().onSave(false);
		},

		handleProdSearch: function (oEvent) {

			var oModelProd = this.getView().getModel("PROD");

			var oProdId = sap.ui.getCore().byId("idProdId").getValue();
			var oProdDes = sap.ui.getCore().byId("idProdDesc").getValue();
			var oCatId = sap.ui.getCore().byId("idCatId").getValue();
			var oCatDes = sap.ui.getCore().byId("idCatDesc").getValue();
			var oRow = sap.ui.getCore().byId("idProdRows").getValue();

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];
			if (oProdId !== "") {
				var oFilter = new Filter("ProductId", FilterOperator.EQ, oProdId);
				oFilters.push(oFilter);
			}
			if (oProdDes !== "") {
				oFilter = new Filter("ShortText", FilterOperator.EQ, oProdDes);
				oFilters.push(oFilter);
			}
			if (oCatId !== "") {
				oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
				oFilters.push(oFilter);
			}
			if (oCatDes !== "") {
				oFilter = new Filter("CategoryDesc", FilterOperator.EQ, oCatDes);
				oFilters.push(oFilter);
			}
			if (oRow !== "") {
				oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
				oFilters.push(oFilter);
			}

			oBusyDialog.open();
			oModelProd.read("/productSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					this.setOwnerModelProperty("search", "/oProd", oData.results);
					oBusyDialog.close();
				}.bind(this),
				error: function (oError) {
					oBusyDialog.close();
				}.bind(this)
			});

		},
		handleProdListItemPress: function (oEvent) {
			var oInd = oEvent.getParameter("listItem").getBindingContextPath().split("/")[2];
			var oProd = sap.ui.getCore().byId("ProdTabId").getItems()[oInd].getCells()[0].getText();
			this.getSource().setValue(oProd);
			this.onClose();
		},
		onCatValueHelp: function (oEvent) {
			this.setOwnerModelProperty("headersearch", "/", {});
			this.setOwnerModelProperty("search", "/", {});
			this.getView().getController().open("CatSearch").setSource(oEvent.getSource());

		},
		handleCatSearch: function () {

			var oModelProd = this.getView().getModel("PROD");

			var oCatId = sap.ui.getCore().byId("idCategId").getValue();
			var oCatDes = sap.ui.getCore().byId("idCategDesc").getValue();
			var oHyrId = sap.ui.getCore().byId("idHyrType").getSelectedKey();
			var oHyrDes = sap.ui.getCore().byId("idHyrDes").getValue();
			var oRow = sap.ui.getCore().byId("idCatRows").getValue();

			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];
			if (oCatId !== "") {
				var oFilter = new Filter("CategoryId", FilterOperator.EQ, oCatId);
				oFilters.push(oFilter);
			}
			if (oCatDes !== "") {
				oFilter = new Filter("CategoryText", FilterOperator.EQ, oCatDes);
				oFilters.push(oFilter);
			}
			if (oHyrId !== "") {
				oFilter = new Filter("HierarchyId", FilterOperator.EQ, oHyrId);
				oFilters.push(oFilter);
			}
			if (oHyrDes !== "") {
				oFilter = new Filter("HierarchyText", FilterOperator.EQ, oHyrDes);
				oFilters.push(oFilter);
			}
			if (oRow !== "") {
				oFilter = new Filter("MaxRows", FilterOperator.EQ, oRow);
				oFilters.push(oFilter);
			}

			// var that = this;
			oBusyDialog.open();
			oModelProd.read("/categorySearchSet", {
				filters: oFilters,
				success: function (oData, response) {

					this.setOwnerModelProperty("search", "/oCat", oData.results);
					oBusyDialog.close();
				}.bind(this),
				error: function (oError) {
					oBusyDialog.close();
				}
			});

		},

		handleCatListItemPress: function (oEvent) {
			var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
			var oPartner = sap.ui.getCore().byId("CatTabId").getItems()[oInd].getCells()[0].getText();
			this.getOwnerComponent().getModel("headersearch").setData({
				CatId: oPartner
			});
			this.onClose();
		},

		handleCustomerSearch: function () {

			var oModelCust = this.getView().getModel("CUST");

			var oPartner = sap.ui.getCore().byId("idCustIdent").getValue();
			var oAd = sap.ui.getCore().byId("idCustName").getValue();
			var oSoyad = sap.ui.getCore().byId("idCustLastName").getValue();
			var oTel = sap.ui.getCore().byId("idCustTel").getValue();
			var oTur = sap.ui.getCore().byId("idCustType").getSelectedKey();
			var oTc = sap.ui.getCore().byId("idCustTc").getValue();
			var oBusyDialog = new sap.m.BusyDialog();

			var oFilters = [];
			if (oPartner !== "") {
				var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
				oFilters.push(oFilter);
			}
			if (oAd !== "") {
				oFilter = new Filter("Firstname", FilterOperator.EQ, oAd);
				oFilters.push(oFilter);
			}
			if (oSoyad !== "") {
				oFilter = new Filter("Lastname", FilterOperator.EQ, oSoyad);
				oFilters.push(oFilter);
			}
			if (oTel !== "") {
				oFilter = new Filter("Telephonemob", FilterOperator.EQ, oTel);
				oFilters.push(oFilter);
			}
			if (oTur !== "") {
				oFilter = new Filter("Partnertype", FilterOperator.EQ, oTur);
				oFilters.push(oFilter);
			}

			if (oTc !== "") {
				oFilter = new Filter("Tckn", FilterOperator.EQ, oTc);
				oFilters.push(oFilter);
			}
			oBusyDialog.open();
			oModelCust.read("/searchCustomerSet", {
				filters: oFilters,
				success: function (oData, response) {
					this.setOwnerModelProperty("search", "/oCustomer", oData.results);

					oBusyDialog.close();
				}.bind(this),
				error: function (oError) {
					oBusyDialog.close();
				}
			});

		},

		handleCustomerListItemPress: function (oEvent) {
			var oInd = oEvent.getParameter("listItem").getBindingContextPath().split('/')[2];
			var oPartner = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[0].getText();
			var oPartnerName = sap.ui.getCore().byId("CustTabId").getItems()[oInd].getCells()[1].getText();
			this.setOwnerModelProperty("selected", "/Custid", oPartner);
			this.setOwnerModelProperty("selected", "/Custidname", oPartnerName);
			// this.getView().byId("inpu2").setValue(oPartner);
			// this.getView().byId("inpu201").setValue(oPartnerName);
			// this.getView().byId("inputPartnerId").setValue(oPartner);
			this.onClose();
		},

		handleCustEnter: function () {
			var oPartner = this.getView().byId("inpu2").getValue();
			var oModelCust = this.getView().getModel("CUST");
			var oFilters = [];

			if (oPartner !== "") {
				var oFilter = new Filter("Partner", FilterOperator.EQ, oPartner);
				oFilters.push(oFilter);
			}

			var that = this;

			oModelCust.read("/searchCustomerSet", {
				filters: oFilters,
				success: function (oData, response) {
					if (oData.results.length > 0) {
						var oPartnerName = oData.results[0].Firstname + " " + oData.results[0].Lastname;
						that.getView().byId("inpu201").setValue(oPartnerName);
					} else {
						that.getView().byId("inpu201").setValue("");
					}
				}

			});

		},
		handleCreateCustomer: function (oEvent) {
			var oCore = sap.ui.getCore();

			this.open("CreateCustomer").setSource(oEvent.getSource());
			// if (!this.CustomerCreate) {
			// 	this.CustomerCreate = sap.ui.xmlfragment("zcrm.zcrm_inquire_price.fragments.CreateCustomer", this);
			// 	this.getView().addDependent(this.CustomerCreate);
			// 	this.CustomerCreate.open();
			// } else {
			// 	this.CustomerCreate.open();
			// }
			oCore.byId("idCustomerType").setSelectedKey("1");
			oCore.byId("idCustomerTax").setVisible(false);
			oCore.byId("idCustomerTaxNo").setVisible(false);
			oCore.byId("idCustCompanyName").setVisible(false);

			var oModelCust = this.getView().getModel("CUST");
			var oView = this.getView();
			var oFilters = [];
			var oFilter = {};
			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0012");
			oFilters.push(oFilter);
			oFilter = new Filter("Extension", FilterOperator.EQ, "1");
			oFilters.push(oFilter);

			oModelCust.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList8 = new sap.ui.model.json.JSONModel(oData);
					this.getOwnerComponent().setModel(oModelJsonList8, "oCustGrup");
				}.bind(this),
				error: function (oError) {}
			});

			var oModelCust = this.getView().getModel("CUST");

			oModelCust.read("/searchHelpCountrySet", {
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					oModelJsonList.setSizeLimit(500);
					this.getOwnerComponent().setModel(oModelJsonList, "oUlke");
				}.bind(this)
			});

			oFilters = [];
			oFilter = new Filter("Land1", FilterOperator.EQ, "TR");
			oFilters.push(oFilter);
			oModelCust.read("/searchHelpRegionSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					this.getOwnerComponent().setModel(oModelJsonList, "oSehir");
				}.bind(this)
			});

			oFilters = [];
			oFilters.push(oFilter);
			oModelCust.read("/searchHelpNationalitySet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					oModelJsonList.setSizeLimit(500);
					this.getOwnerComponent().setModel(oModelJsonList, "oNat");
				}.bind(this)
			});

			oFilters = [];
			oFilter = new Filter("Code", FilterOperator.EQ, "0013");
			oFilters.push(oFilter);
			oModelCust.read("/valueHelpSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					this.getOwnerComponent().setModel(oModelJsonList, "oOcc");
				}.bind(this)
			});

		},
		onCustomerCreateSave: function () {
			debugger;
			var oModelCust = this.getView().getModel("CUST");
			var oCore = sap.ui.getCore();

			if (this.custAlanKontrol()) {
				var oCustomer = {};

				oCustomer.Lifecyclestage = oCore.byId("idCustomerRole").getSelectedKey();
				oCustomer.BuGroup = oCore.byId("idGroup").getSelectedKey();
				oCustomer.Partnertype = oCore.byId("idCustomerType").getSelectedKey();

				if (oCustomer.Partnertype === "1") // Bireysel müşteri
				{
					oCustomer.Firstname = oCore.byId("idCustomerName").getValue();
					oCustomer.Lastname = oCore.byId("idCustomerSurName").getValue();
					oCustomer.TaxNum = oCore.byId("idCustomerID").getValue();
				} else {
					oCustomer.TaxNum = oCore.byId("idCustomerTaxNo").getValue();
					oCustomer.Name1 = oCore.byId("idCustCompanyName").getValue();
					oCustomer.TaxCenter = oCore.byId("idCustomerTax").getValue();
				}

				oCustomer.Sex = oCore.byId("idCustomerSex").getSelectedKey();
				oCustomer.Nationality = oCore.byId("idCustomerNat").getSelectedKey();
				oCustomer.Occupation = oCore.byId("idCustomerOcc").getSelectedKey();

				var oAdres = {};
				oAdres.StrSuppl1 = oCore.byId("idCustomerAdres").getValue();
				oAdres.StrSuppl3 = oCore.byId("idCustomerAdres2").getValue();
				oAdres.Telephonemob = oCore.byId("idCustMobileNumber").getValue();
				oAdres.Country = oCore.byId("idCustomerCountry").getSelectedKey();
				oAdres.Region = oCore.byId("idCustomerCity").getSelectedKey();
				if(oCore.byId("idCustomerDist").getSelectedItem()){
				oAdres.City = oCore.byId("idCustomerDist").getSelectedItem().getText();}
				oAdres.CityNo = oCore.byId("idCustomerDist").getSelectedKey();
				oAdres.EMailsmt = oCore.byId("idCustomerEmail").getValue();

				oCustomer.ToAddress = oAdres;

				var oPermission = [];

				oPermission.push({
					Partner: "",
					Zzfld00003iTxt: "Hepsi",
					Zzfld00003i: "HEP",
					Zzfld00003jTxt: "Verilmedi",
					Zzfld00003j: "002",
					Zzfld00003k: "",
					Zzfld00003l: new Date(),
					Zzfld00003m: "",
					Zzfld00003n: ""
				});

				oCustomer.ToPermissionSet = oPermission;

				var that = this;
				var oBusyDialog = new sap.m.BusyDialog();

				oBusyDialog.open();
				oModelCust.create("/customerHeaderSet", oCustomer, {
					success: function (oData, oResponse) {
						// oModelJsonList.setData(oData);
						// that.getView().byId("inpu2").setValue(oData.Partner);
						this.setOwnerModelProperty("selected", "/Custid", oData.Partner);
						if (oData.FirstName !== "") // Bireysel müşteri
						{
							this.setOwnerModelProperty("selected", "/Custidname", oCustomer.Firstname + " " + oCustomer.Lastname);
							// that.getView().byId("inpu201").setValue(oCustomer.Firstname + oCustomer.Lastname);
						} else {
							this.setOwnerModelProperty("selected", "/Custidname", oCustomer.Name1);
							// that.getView().byId("inpu201").setValue(oCustomer.Name1);
						}
						oBusyDialog.close();
						this.onClose();
						this.getView().getController().showMessageBox("Müşteri yaratıldı");
					}.bind(this),
					error: function (oError) {
						oBusyDialog.close();
						this.getView().getController().showMessageBox(oError);
						// sap.m.MessageBox.show(message, {
						// 	icon: sap.m.MessageBox.Icon.ERROR,
						// 	title: "HATA",
						// 	actions: [sap.m.MessageBox.Action.OK]
						// });
					}.bind(this)
				});

			} else {
				var oMessage = "Lütfen Tüm Zorunlu Alanları Doldurunuz";
				this.getView().getController().showMessageBox(oMessage, "warning");
				// sap.m.MessageBox.show(oMessage, {
				// 	icon: sap.m.MessageBox.Icon.ERROR,
				// 	title: "UYARI",
				// 	actions: [sap.m.MessageBox.Action.OK]
				// });
			}

		},

		custAlanKontrol: function () {
			var oCore = sap.ui.getCore();
			var oVal = true;

			// if (oCore.byId("idGroup").getSelectedKey() === "" && oCore.byId("idGroup").getVisible()) {
			// 	oCore.byId("idGroup").setValueState(sap.ui.core.ValueState.Error);
			// 	oVal = false;
			// } else {
			// 	oCore.byId("idGroup").setValueState(sap.ui.core.ValueState.None);
			// }

			if (oCore.byId("idCustomerRole").getSelectedKey() === "" && oCore.byId("idCustomerRolelbl").getRequired()) {
				oCore.byId("idCustomerRole").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerRole").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerName").getValue() === "" && oCore.byId("idCustomerNamelbl").getRequired()) {
				oCore.byId("idCustomerName").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerName").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerSurName").getValue() === "" && oCore.byId("idCustomerSurNamelbl").getRequired()) {
				oCore.byId("idCustomerSurName").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerSurName").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustCompanyName").getValue() === "" && oCore.byId("idCustCompanyNamelbl").getRequired()) {
				oCore.byId("idCustCompanyName").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustCompanyName").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustMobileNumber").getValue() === "" && oCore.byId("idCustMobileNumberlbl").getRequired()) {
				oCore.byId("idCustMobileNumber").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustMobileNumber").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idGroup").getValue() === "" && oCore.byId("idGrouplbl").getRequired()) {
				oCore.byId("idGroup").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idGroup").setValueState(sap.ui.core.ValueState.None);
			}
			if (oCore.byId("idCustomerID").getValue() === "" && oCore.byId("idCustomerIDlbl").getRequired()) {
				oCore.byId("idCustomerID").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerID").setValueState(sap.ui.core.ValueState.None);
			}
			if (oCore.byId("idCustomerTax").getValue() === "" && oCore.byId("idCustomerTaxlbl").getRequired()) {
				oCore.byId("idCustomerTax").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerTax").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerTaxNo").getValue() === "" && oCore.byId("idCustomerTaxNolbl").getRequired()) {
				oCore.byId("idCustomerTaxNo").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerTaxNo").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerAdres").getValue() === "" && oCore.byId("idCustomerAdreslbl").getRequired()) {
				oCore.byId("idCustomerAdres").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerAdres").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerCountry").getValue() === "" && oCore.byId("idCustomerCountrylbl").getRequired()) {
				oCore.byId("idCustomerCountry").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerCountry").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerCity").getValue() === "" && oCore.byId("idCustomerCitylbl").getRequired()) {
				oCore.byId("idCustomerCity").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerCity").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerDist").getValue() === "" && oCore.byId("idCustomerDistlbl").getRequired()) {
				oCore.byId("idCustomerDist").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerDist").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerEmail").getValue() === "" && oCore.byId("idCustomerEmaillbl").getRequired()) {
				oCore.byId("idCustomerEmail").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerEmail").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerSex").getValue() === "" && oCore.byId("idCustomerSexlbl").getRequired()) {
				oCore.byId("idCustomerSex").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerSex").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerNat").getValue() === "" && oCore.byId("idCustomerNatlbl").getRequired()) {
				oCore.byId("idCustomerNat").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerNat").setValueState(sap.ui.core.ValueState.None);
			}

			if (oCore.byId("idCustomerOcc").getValue() === "" && oCore.byId("idCustomerOcclbl").getRequired()) {
				oCore.byId("idCustomerOcc").setValueState(sap.ui.core.ValueState.Error);
				oVal = false;
			} else {
				oCore.byId("idCustomerOcc").setValueState(sap.ui.core.ValueState.None);
			}

			return oVal;
		},

		onCountryChange: function () {
			var oView = this.getView();
			var oModelCust = this.getView().getModel("CUST");
			var oCore = sap.ui.getCore();
			var oCountry = oCore.byId("idCustomerCountry").getSelectedKey();
			var oFilters = [];
			var oFilter = {};

			oCore.byId("idCustomerCity").setSelectedKey("");
			oCore.byId("idCustomerDist").setSelectedKey("");

			oFilters = [];
			oFilter = new Filter("Land1", FilterOperator.EQ, oCountry);
			oFilters.push(oFilter);

			oModelCust.read("/searchHelpRegionSet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					this.getOwnerComponent().setModel(oModelJsonList, "oSehir");
				}.bind(this)
			});

		},
		onCityChange: function () {
			var oView = this.getView();
			var oModelCust = this.getView().getModel("CUST");
			var oCore = sap.ui.getCore();
			var oCountry = oCore.byId("idCustomerCountry").getSelectedKey();
			var oCity = oCore.byId("idCustomerCity").getSelectedKey();

			var oFilters = [];
			var oFilter = {};

			oCore.byId("idCustomerDist").setSelectedKey("");

			oFilters = [];
			oFilter = new Filter("Country", FilterOperator.EQ, oCountry);
			oFilters.push(oFilter);
			oFilter = new Filter("Region", FilterOperator.EQ, oCity);
			oFilters.push(oFilter);

			oModelCust.read("/searchHelpCitySet", {
				filters: oFilters,
				success: function (oData, response) {
					var oModelJsonList = new sap.ui.model.json.JSONModel(oData);
					oModelJsonList.setData(oData);
					this.getOwnerComponent().setModel(oModelJsonList, "oIlce");
				}.bind(this)
			});

		},
		onCustomerTypeChange: function () {
			var oCore = sap.ui.getCore();

			var oSelectedType = oCore.byId("idCustomerType").getSelectedKey();
			var oSelectedRole = oCore.byId("idCustomerRole").getSelectedKey();

			// Bireysel & Gerçek
			if (oSelectedType === "1" && oSelectedRole === "CRM000") {
				oCore.byId("idCustomerName").setVisible(true);
				oCore.byId("idCustomerNamelbl").setRequired(true);
				oCore.byId("idCustomerSurName").setVisible(true);
				oCore.byId("idCustomerSurNamelbl").setRequired(true);
				oCore.byId("idCustomerID").setVisible(true);
				oCore.byId("idCustomerIDlbl").setRequired(true);
				oCore.byId("idCustMobileNumber").setVisible(true);
				oCore.byId("idCustMobileNumberlbl").setRequired(true);
				oCore.byId("idCustomerAdres").setVisible(true);
				oCore.byId("idCustomerAdreslbl").setRequired(true);
				oCore.byId("idCustomerCountry").setVisible(true);
				oCore.byId("idCustomerCountrylbl").setRequired(true);
				oCore.byId("idCustomerCity").setVisible(true);
				oCore.byId("idCustomerCitylbl").setRequired(true);
				oCore.byId("idCustomerDist").setVisible(true);
				oCore.byId("idCustomerDistlbl").setRequired(true);
				oCore.byId("idCustomerEmail").setVisible(true);
				oCore.byId("idCustomerEmaillbl").setRequired(true);
				oCore.byId("idCustomerSex").setVisible(true);
				oCore.byId("idCustomerSexlbl").setRequired(true);
				oCore.byId("idCustomerNat").setVisible(true);
				oCore.byId("idCustomerNatlbl").setRequired(true);
				oCore.byId("idCustomerOcc").setVisible(true);
				oCore.byId("idCustomerOcclbl").setRequired(true);

				oCore.byId("idCustomerTax").setVisible(false);
				oCore.byId("idCustomerTaxlbl").setRequired(false);
				oCore.byId("idCustomerTaxNo").setVisible(false);
				oCore.byId("idCustomerTaxNolbl").setRequired(false);
				oCore.byId("idCustCompanyName").setVisible(false);
				oCore.byId("idCustCompanyNamelbl").setRequired(false);

			}
			// Bireysel & Potansiyel
			else if (oSelectedType === "1" && oSelectedRole === "BUP002") {
				oCore.byId("idCustomerName").setVisible(true);
				oCore.byId("idCustomerNamelbl").setRequired(true);
				oCore.byId("idCustomerSurName").setVisible(true);
				oCore.byId("idCustomerSurNamelbl").setRequired(true);
				oCore.byId("idCustomerID").setVisible(true);
				oCore.byId("idCustomerIDlbl").setRequired(false);
				oCore.byId("idCustMobileNumber").setVisible(true);
				oCore.byId("idCustMobileNumberlbl").setRequired(true);
				oCore.byId("idCustomerAdres").setVisible(true);
				oCore.byId("idCustomerAdreslbl").setRequired(false);
				oCore.byId("idCustomerCountry").setVisible(true);
				oCore.byId("idCustomerCountrylbl").setRequired(false);
				oCore.byId("idCustomerCity").setVisible(true);
				oCore.byId("idCustomerCitylbl").setRequired(false);
				oCore.byId("idCustomerDist").setVisible(true);
				oCore.byId("idCustomerDistlbl").setRequired(false);
				oCore.byId("idCustomerEmail").setVisible(true);
				oCore.byId("idCustomerEmaillbl").setRequired(false);
				oCore.byId("idCustomerSex").setVisible(true);
				oCore.byId("idCustomerSexlbl").setRequired(true);
				oCore.byId("idCustomerNat").setVisible(true);
				oCore.byId("idCustomerNatlbl").setRequired(true);
				oCore.byId("idCustomerOcc").setVisible(true);
				oCore.byId("idCustomerOcclbl").setRequired(false);

				oCore.byId("idCustomerTax").setVisible(false);
				oCore.byId("idCustomerTaxlbl").setRequired(false);
				oCore.byId("idCustomerTaxNo").setVisible(false);
				oCore.byId("idCustomerTaxNolbl").setRequired(false);
				oCore.byId("idCustCompanyName").setVisible(false);
				oCore.byId("idCustCompanyNamelbl").setRequired(false);

			}
			// Kurumsal & Gerçek
			else if (oSelectedType === "2" && oSelectedRole === "CRM000") {
				oCore.byId("idCustomerName").setVisible(false);
				oCore.byId("idCustomerNamelbl").setRequired(false);
				oCore.byId("idCustomerSurName").setVisible(false);
				oCore.byId("idCustomerSurNamelbl").setRequired(false);
				oCore.byId("idCustomerID").setVisible(false);
				oCore.byId("idCustomerIDlbl").setRequired(false);
				oCore.byId("idCustomerEmail").setVisible(true);
				oCore.byId("idCustomerEmaillbl").setRequired(false);
				oCore.byId("idCustomerSex").setVisible(false);
				oCore.byId("idCustomerSexlbl").setRequired(false);
				oCore.byId("idCustomerNat").setVisible(false);
				oCore.byId("idCustomerNatlbl").setRequired(false);
				oCore.byId("idCustomerOcc").setVisible(false);
				oCore.byId("idCustomerOcclbl").setRequired(false);

				oCore.byId("idCustMobileNumber").setVisible(true);
				oCore.byId("idCustMobileNumberlbl").setRequired(true);
				oCore.byId("idCustomerTax").setVisible(true);
				oCore.byId("idCustomerTaxlbl").setRequired(true);
				oCore.byId("idCustomerTaxNo").setVisible(true);
				oCore.byId("idCustomerTaxNolbl").setRequired(true);
				oCore.byId("idCustCompanyName").setVisible(true);
				oCore.byId("idCustCompanyNamelbl").setRequired(true);
				oCore.byId("idCustomerAdres").setVisible(true);
				oCore.byId("idCustomerAdreslbl").setRequired(true);
				oCore.byId("idCustomerCountry").setVisible(true);
				oCore.byId("idCustomerCountrylbl").setRequired(true);
				oCore.byId("idCustomerCity").setVisible(true);
				oCore.byId("idCustomerCitylbl").setRequired(true);
				oCore.byId("idCustomerDist").setVisible(true);
				oCore.byId("idCustomerDistlbl").setRequired(true);
			}
			// Kurumsal & Potansiyel
			else if (oSelectedType === "2" && oSelectedRole === "BUP002") {
				oCore.byId("idCustomerName").setVisible(false);
				oCore.byId("idCustomerNamelbl").setRequired(false);
				oCore.byId("idCustomerSurName").setVisible(false);
				oCore.byId("idCustomerSurNamelbl").setRequired(false);
				oCore.byId("idCustomerID").setVisible(false);
				oCore.byId("idCustomerIDlbl").setRequired(false);
				oCore.byId("idCustomerEmail").setVisible(true);
				oCore.byId("idCustomerEmaillbl").setRequired(false);
				oCore.byId("idCustomerSex").setVisible(false);
				oCore.byId("idCustomerSexlbl").setRequired(false);
				oCore.byId("idCustomerNat").setVisible(false);
				oCore.byId("idCustomerNatlbl").setRequired(false);
				oCore.byId("idCustomerOcc").setVisible(false);
				oCore.byId("idCustomerOcclbl").setRequired(false);

				oCore.byId("idCustMobileNumber").setVisible(true);
				oCore.byId("idCustMobileNumberlbl").setRequired(true);
				oCore.byId("idCustomerTax").setVisible(true);
				oCore.byId("idCustomerTaxlbl").setRequired(false);
				oCore.byId("idCustomerTaxNo").setVisible(true);
				oCore.byId("idCustomerTaxNolbl").setRequired(false);
				oCore.byId("idCustCompanyName").setVisible(true);
				oCore.byId("idCustCompanyNamelbl").setRequired(true);
				oCore.byId("idCustomerAdres").setVisible(true);
				oCore.byId("idCustomerAdreslbl").setRequired(false);
				oCore.byId("idCustomerCountry").setVisible(true);
				oCore.byId("idCustomerCountrylbl").setRequired(false);
				oCore.byId("idCustomerCity").setVisible(true);
				oCore.byId("idCustomerCitylbl").setRequired(false);
				oCore.byId("idCustomerDist").setVisible(true);
				oCore.byId("idCustomerDistlbl").setRequired(false);
			}

			this.custAlanKontrol();

		},
		onTakim: function (oEvent) {
			this.setOwnerModelProperty("selected", "/Model", []);
			this.setOwnerModelProperty("selected", "/KatalogModel", "");
			var oCode = "0021";
			var oExtension = oEvent.getSource().getSelectedKey();
			this.getView().getController().BaseValueHelp("/valueHelpSet", "/KatalogModel", {
				Code: oCode,
				Extension: oExtension
			});
		},
		handleSelectCatSearch: function (oEvent) {

			var oModelProd = this.getView().getModel("PROD");

			var oHead = this.getView().getModel("oModel").getProperty("/");
			var oTakim = this.getOwnerModelProperty("selected", "/KatalogTakım");
			var KatalogModel = this.getOwnerModelProperty("selected", "/KatalogModel");
			var oModelTnm = this.getOwnerModelProperty("selected", "/KatalogModelTnm");
			var oUniteUrunKod = this.getOwnerModelProperty("selected", "/KatalogUniteUrunKod");
			var oUrunId = this.getOwnerModelProperty("selected", "/KatalogUrunId");
			var oCategId = this.getOwnerModelProperty("selected", "/CategId");
			var oConfigEnable = this.getOwnerModelProperty("selected", "/ConfigEnable");

			var oFilters = [];

			if (oTakim) {
				oFilters.push(new Filter({
					path: 'Zztakim',
					value1: oTakim,
					operator: FilterOperator.EQ
				}));
			}

			if (oModelTnm) {
				oFilters.push(new Filter({
					path: 'CategoryDesc',
					value1: oModelTnm,
					operator: FilterOperator.EQ
				}));
			}
			if (oUniteUrunKod) {
				oFilters.push(new Filter({
					path: 'Description',
					value1: oUniteUrunKod,
					operator: FilterOperator.EQ
				}));
			}
			if (KatalogModel) {
				oFilters.push(new Filter({
					path: 'Zzmodel',
					value1: KatalogModel,
					operator: FilterOperator.EQ
				}));
			}
			if (oUrunId) {
				oFilters.push(new Filter({
					path: 'ProductId',
					value1: oUrunId,
					operator: FilterOperator.EQ
				}));
			}
			if (oCategId) {
				oFilters.push(new Filter({
					path: 'CategoryId',
					value1: oCategId,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ProcessType) {
				oFilters.push(new Filter({
					path: 'ProcessType',
					value1: oHead.ProcessType,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceList) {
				oFilters.push(new Filter({
					path: 'PriceList',
					value1: oHead.ToPricing.PriceList,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.Currency) {
				oFilters.push(new Filter({
					path: 'Currency',
					value1: oHead.ToPricing.Currency,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceDate) {
				oFilters.push(new Filter({
					path: 'PriceDate',
					value1: oHead.ToPricing.PriceDate,
					operator: FilterOperator.EQ
				}));
			}

			BusyIndicator.show();
			oModelProd.read("/productCatalogSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					this.setOwnerModelProperty("search", "/oCatalog", oData.results);
					sap.ui.getCore().byId("SelProdCatTabId").removeSelections();
					BusyIndicator.hide();
				}.bind(this),
				error: function (oError) {
					BusyIndicator.hide();
				}
			});

		},
		handleSelectCatAdd: function (oEvent) {

			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oConfig = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
				// var oIndirim = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: "",
					Quantity: "1.000",
					ProcessQtyUnit: "ADT",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: oConfig,
					// Zzfld000083: oKalemNo,
					// ZzaTeshirblgno: oSipNo,
					// ZzTeshirind: oIndirim
				};

				List.push(oRow);

			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			sap.ui.getCore().byId("SelProdCatTabId").removeSelections();
			oTab.getModel("oModel").refresh();
			//			this._oSelProdCatHelpDialog.close();
		},
		handleSelectProdTree: function (oEvent) {

			// Ürün ağacından seç arama yardımı burada ürün katoloğundan seçilen satırlar kullanıarak ürün ağacı getiririlir.
			var oModelProd = this.getView().getModel("PROD");
			var oHead = this.getView().getModel("oModel").getProperty("/");
			var oSelProdTab = sap.ui.getCore().byId("SelProdCatTabId");
			var oSelected = oSelProdTab.getSelectedItems();
			var oFilterA = [];
			var oFilterProd = [];

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[4].getText();
				if (oUrunKod) {
					oFilterProd.push(new Filter({
						path: 'ProductId',
						value1: oUrunKod,
						operator: FilterOperator.EQ,
						and: false
					}));
				}
			}

			var oFilterOR = new Filter({
				filters: oFilterProd,
				and: false,
			});

			if (oHead.ProcessType) {
				oFilterA.push(new Filter({
					path: 'ProcessType',
					value1: oHead.ProcessType,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceList) {
				oFilterA.push(new Filter({
					path: 'PriceList',
					value1: oHead.ToPricing.PriceList,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.Currency) {
				oFilterA.push(new Filter({
					path: 'Currency',
					value1: oHead.ToPricing.Currency,
					operator: FilterOperator.EQ
				}));
			}
			if (oHead.ToPricing.PriceDate) {
				oFilterA.push(new Filter({
					path: 'PriceDate',
					value1: oHead.ToPricing.PriceDate,
					operator: FilterOperator.EQ
				}));
			}

			var oFilterAND = new Filter({
				filters: oFilterA,
				and: true
			});
			var oFilters = [new Filter({
				and: true,
				filters: [oFilterOR, oFilterAND]

			})];

			var oBusyDialog = new sap.m.BusyDialog();
			var oModelJsonList = new sap.ui.model.json.JSONModel();
			BusyIndicator.show();
			oModelProd.read("/productCatalogTreeSearchSet", {
				filters: oFilters,
				success: function (oData, response) {
					this.setOwnerModelProperty("search", "/oProdTree", oData.results);
					BusyIndicator.hide();
				}.bind(this),
				error: function (oError) {
					BusyIndicator.hide();
				}
			});

			this.open("SelectFromProdTree").setSource(oEvent.getSource());

		},
		handleSelectProdTreeAdd: function (oEvent) {

			var oTab = this.getView().byId("SepetTabId");
			var List = oTab.getModel("oModel").getProperty("/ToItems/results");
			var oSelProdTab = sap.ui.getCore().byId("SelProdTreeTabId");
			var oSelected = oSelProdTab.getSelectedItems();

			for (var i = 0; i < oSelected.length; i++) {

				var oInd = oSelected[i].getBindingContextPath().split('/')[2];
				var oUrunKod = oSelProdTab.getItems()[oInd].getCells()[0].getText();
				var oText = oSelProdTab.getItems()[oInd].getCells()[1].getText();
				var oQuantity = oSelProdTab.getItems()[oInd].getCells()[5].getText();

				var oRow = {
					OrderedProd: oUrunKod,
					Description: oText,
					Quantity: oQuantity,
					ProcessQtyUnit: "",
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					Currency: "",
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: ""
				};
				List.push(oRow);
			}

			oTab.getModel("oModel").setProperty("/ToItems/results", List);
			oTab.getModel("oModel").refresh();
			this.onClose();
		},
		onFollowupItemNav: function () {
			var oModel = this.getView().getModel("oModel");
			var sguid = oModel.getProperty("/Guid");
			var oId = this.getView().getModel("selected").getProperty("/ProcessType");
			var aSelectedContext = sap.ui.getCore().byId("FollowupItemTable").getSelectedContexts();
			var date = oModel.getProperty("/ToSales/ReqDlvDate");
			var aSelectedItems = aSelectedContext.map(function (item) {
				var oItem = item.getObject();
				return {
					OrderedProd: oItem.OrderedProd,
					OrderedProdDesc: oItem.OrderedProdDesc,
					Quantity: oItem.Quantity,
					ProcessQtyUnit: oItem.ProcessQtyUnit,
					Currency: "TRY",
					ObjtypeA: oId,
					PreviousItemGuid: oItem.Guid,
					Conditionamount1: "0.000",
					Brmfytkdvli: "0.000",
					KampInd: "0.000",
					ThslatInd: "0.000",
					Zzfld00008x: "0.000",
					TeslimatTrh: date,
					Zzerpstatus: "",
					Zzspecad: "",
					Zzteshirdepo: "",
					ShowConfigButton: "X"

				};
			});

			sap.ui.getCore().setModel(this.getView().getModel("oModel"), "FollowupModel");

			var crossData = this.getOwnerComponent().resetFields(this.getView().getModel("oModel").oData, {
				Guid: "00000000-0000-0000-0000-000000000000",
				ProcessType: oId,
				ToItems: {
					results: aSelectedItems
				},
				ToPricing: {
					PriceList: oModel.getProperty("/ToPricing/PriceList"),
					Currency: oModel.getProperty("/ToPricing/Currency"),
					PriceDate: new Date()
				},
				PrevObjectGuid: sguid,
				ToCustomerExt: {
					Zzfld00009x: new Date(),
					Zzfld0000ag: "",
					ZzodemeTipi: oModel.getProperty("/ToCustomerExt/ZzodemeTipi")

					// ZzSptiskonto: oModel.getProperty("/ToCustomerExt/ZzSptiskonto")

				},
				ToCondEasyEntries: {
					CondAmount3: oModel.getProperty("/ToCustomerExt/ZzSptiskonto")
				},

				ToConfig: {
					results: oModel.getProperty("/ToConfig/results")
				}

			}, /*reset fields*/ ["ToCumulate", "ObjectId", "ToPricing", "ToTahsilat", "ShipCond", "ShipCondTxt"]);

			if (oId === "ZS20") {
				debugger;
				crossData.ToCustomerExt.ZzSptiskonto = crossData.ToCondEasyEntries.CondAmount3;
			}

			sap.ui.getCore().setModel(new sap.ui.model.json.JSONModel({
				sendToSiparis: crossData
			}), "crossData");
			var xnavservice = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService && sap.ushell.Container.getService(
				"CrossApplicationNavigation");

			xnavservice.toExternal({
				target: {
					semanticObject: "ZCRM_SALES",
					action: "display"
				}
			});

			sap.ui.getCore().setModel(this.getView().getModel("oModel"), "oModelGlobal");

			// var FollowupModel = sap.ui.getCore().getModel("FollowupModel");
			// FollowupModel.setProperty("/ToItems/results", aSelectedItems);
			// FollowupModel.setProperty("/ToCumulate", {});
			// FollowupModel.setProperty("/ObjectId", "");
			// FollowupModel.setProperty("/Guid", "00000000-0000-0000-0000-000000000000");
			// FollowupModel.setProperty("/PrevObjectGuid", sguid);
			// FollowupModel.setProperty("/ProcessType", oId);
			// FollowupModel.setProperty("/ToPricing/PriceList", "");
			// FollowupModel.setProperty("/ToPricing/Currency", "");
			// sap.ui.getCore().getModel("FollowupModel").setProperty("/ToPartn"ers", []);
			// sap.ui.getCore().getModel("FollowupModel").setProperty("/ToStatus", {});

			this.getView().getModel("selected").setProperty("/FollowupItems", "X");

			/*var oText = sap.ui.getCore().byId("idOrderTypes").getSelectedButton().getText();

			var oId = oText.substr(oText.length - 4);*/
			this.onClose();

			// var oModel = this.getView().getModel("oModel");
			// var sguid = oModel.getProperty("/Guid");

			// sap.ui.core.UIComponent.getRouterFor(this.getView()).navTo("CreateEdit", {
			// 	guid: "00000000-0000-0000-0000-000000000000",
			// 	orderType: oId
			// });

		},
		handleOrderTypeSelect: function (oEvent) {
			// if (this.getSource().getId().endsWith("-NewDoc")) {
			// 	var oText = sap.ui.getCore().byId("idOrderTypes").getSelectedButton().getText();

			// 	var oId = oText.substr(oText.length - 4);
			// 	this.onClose();

			// 	sap.ui.core.UIComponent.getRouterFor(this.getView()).navTo("CreateEdit", {
			// 		guid: "00000000-0000-0000-0000-000000000000",
			// 		orderType: oId
			// 	});

			// } else {
			this.getView().getController().onFollowupItem();
			// }

		}

	});

	return FragmentController;
});